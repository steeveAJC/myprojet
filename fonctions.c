#include <stdio.h>
#include <stdlib.h>
#include "entete.h"

char mention(float vNote)
{

    //char texte[]
    if (vNote >= 16)
        return 'A';
    if (vNote >= 14)
        return 'B';
    if (vNote >= 12)
        return 'C';
    if (vNote >= 10)
        return 'D';
    if (vNote >= 8)
        return 'E';
     return 'F';
}
int majmin(int vAnnee)
{
    return (vAnnee >= 2019-18)?0:1;
}

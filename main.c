#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "entete.h"


int main()
{
   char nom[LG +1]="\0";
   char pren[LG +1]="\0";
   int annee;
   float note;

   printf("\n--------- GESTION DES ELEVES --------------\n");

   printf("\nSaisir un nom : ");
   __fpurge(stdin);
   scanf("%s", nom);

    printf("\nSaisir un prenom : ");
    __fpurge(stdin);
    scanf("%s", pren);

    printf("\nSaisir l'annee de naissance : ");
    __fpurge(stdin);
    scanf("%d", &annee);

    printf("\nSaisir une note (0-20) : ");
    __fpurge(stdin);
    scanf("%f", &note);

    printf("\n%s %s, né(e) en %d\nNote : %f", nom, pren, annee, note);
    printf("\nRésultat : %C", mention(note));


    printf("\nVous êtes : %s", (majmin(annee)==0?"Majeur(e)":"Mineur(e)"));
    return 0;
}
